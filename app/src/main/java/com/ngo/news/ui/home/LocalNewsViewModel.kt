package com.ngo.news.ui.home

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ngo.news.data.model.Category
import com.ngo.news.data.model.GlobalNewsModel
import com.ngo.news.data.model.NewsModel
import com.ngo.news.data.model.RegModel
import com.ngo.news.data.repository.LocalRepository
import com.ngo.news.utils.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class LocalNewsViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var repository: LocalRepository
    private val compositeDisposable = CompositeDisposable()

    val isLoading = ObservableField(false)

    var companiesModel = MutableLiveData<ArrayList<NewsModel>>()
    var companiesModel2 = MutableLiveData<ArrayList<GlobalNewsModel>>()
    var companiesModel3 = MutableLiveData<ArrayList<RegModel>>()
    var companiesModelCategory = MutableLiveData<ArrayList<Category>>()

    fun getNewsByType(type: String, category:String, country:String) {
        repository= LocalRepository()
        compositeDisposable += repository
            .getNewsByType(type = type,category = category,country = country)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val array= ArrayList<NewsModel>()
                for (i in it.size- 1 downTo 0) {
                    array.add(it[i])
                }

                isLoading.set(false)
                companiesModel.value= array

            }, {
                isLoading.set(false)
                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }

    fun search(type: String,text:String) {
        repository= LocalRepository()
        compositeDisposable += repository
            .search(type = type,text = text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val array= ArrayList<NewsModel>()
                for (i in it.size- 1 downTo 0) {
                    array.add(it[i])
                }

                isLoading.set(false)
                companiesModel.value= array

            }, {
                isLoading.set(false)

                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }

    fun searchG(type: String,text:String) {
        repository= LocalRepository()
        compositeDisposable += repository
            .searchG(type = type,text = text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val array= ArrayList<GlobalNewsModel>()
                for (i in it.size- 1 downTo 0) {
                    array.add(it[i])
                }

                isLoading.set(false)
                companiesModel2.value= array

            }, {
                isLoading.set(false)

                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }

    fun searchR(type: String,text:String) {
        repository= LocalRepository()
        compositeDisposable += repository
            .searchR(type = type,text = text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val array= ArrayList<RegModel>()
                for (i in it.size- 1 downTo 0) {
                    array.add(it[i])
                }

                isLoading.set(false)
                companiesModel3.value= array

            }, {
                isLoading.set(false)

                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }

    fun getNewsByType2(type: String, category:String) {
        repository= LocalRepository()
        compositeDisposable += repository
            .getNewsByType2(type = type,category = category)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val array= ArrayList<GlobalNewsModel>()
                for (i in it.size- 1 downTo 0) {
                    array.add(it[i])
                }

                isLoading.set(false)
                companiesModel2.value= array

            }, {
                isLoading.set(false)
                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }

    fun getNewsByType3(type: String, category:String, country:String) {
        repository= LocalRepository()
        compositeDisposable += repository
            .getNewsByType3(type = type,category = category,country = country)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val array= ArrayList<RegModel>()
                for (i in it.size- 1 downTo 0) {
                    array.add(it[i])
                }

                isLoading.set(false)
                companiesModel3.value= array

            }, {
                isLoading.set(false)

                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }

    fun getCategories() {
        repository= LocalRepository()
        compositeDisposable += repository
            .getCategories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<ArrayList<Category>>() {

                override fun onError(e: Throwable) {
                    isLoading.set(false)

                    Log.d("DSbfsdbfdbf", e.message.toString())
                }

                override fun onComplete() {
                    isLoading.set(false)

                }

                override fun onNext(t: ArrayList<Category>) {
                    companiesModelCategory.value= t

                }
            })
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}
