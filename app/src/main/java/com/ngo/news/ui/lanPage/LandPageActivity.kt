package com.ngo.news.ui.lanPage

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ngo.news.R
import com.ngo.news.SplashActivity
import com.ngo.news.ui.base.BaseActivity

class LandPageActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_land_page)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LandPageActivity::class.java)
            context.startActivity(intent)
        }
    }
}