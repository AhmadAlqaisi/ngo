package com.ngo.news.ui.fav

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ngo.news.data.model.BookMarkModel
import com.ngo.news.data.repository.FavRepository
import com.ngo.news.utils.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class BookMarkViewModel (application: Application) : AndroidViewModel(application) {
    lateinit var repository: FavRepository
    private val compositeDisposable = CompositeDisposable()

    val isLoading = ObservableField(false)

    var companiesModel = MutableLiveData<ArrayList<BookMarkModel>>()


    fun getNewsByType(id: String) {
        repository= FavRepository()
        compositeDisposable += repository
            .getNewsByType(id = id )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val array= ArrayList<BookMarkModel>()
                for (i in it.size- 1 downTo 0) {
                    array.add(it[i])
                }

                isLoading.set(false)
                companiesModel.value= array

            },{
                isLoading.set(false)

                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }
}
