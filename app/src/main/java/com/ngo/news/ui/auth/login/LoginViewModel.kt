package com.ngo.news.ui.auth.login

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController
import com.nafith.testtask.utils.toast
import com.ngo.news.R
import com.ngo.news.data.manager.CacheManager
import com.ngo.news.data.model.SignInModel
import com.ngo.news.data.repository.SignInRepository
import com.ngo.news.utils.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LoginViewModel (application: Application) : AndroidViewModel(application)  {

    private lateinit var repository: SignInRepository

    val isLoading = ObservableField(false)
    var isEmail= ObservableField(false)
    var isPassword= ObservableField(false)

    private val compositeDisposable = CompositeDisposable()

    fun back(navController: NavController) {
        navController.navigate(R.id.action_loginFragment_to_chooseAuth)
    }

    fun signUp(navController: NavController) {
        navController.navigate(R.id.action_loginFragment_to_signUp)
    }

    fun signIn(model: SignInModel, context: Context, navController: NavController) {
        repository= SignInRepository()

        isLoading.set(true)

        when {
            model.getEmail().isEmpty() -> { isLoading.set(false); isEmail.set(true) }
            model.getPassword().isEmpty() -> { isLoading.set(false); isPassword.set(true) }

            else -> {

                compositeDisposable += repository
                    .signIn(model = model)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        isLoading.set(false)
                        Log.d("dafsfsdfs", it.toString())
                        CacheManager.setUserInfo(userInfo = it)
                        navController.navigate(R.id.action_loginFragment_to_mainActivity)

                    }, {
                        isLoading.set(false)
                        context.getString(R.string.email_password).toast(context)
                        Log.d("error", it.message.toString())

                    })
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}