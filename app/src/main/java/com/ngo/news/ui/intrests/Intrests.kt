package com.ngo.news.ui.intrests

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mlsdev.animatedrv.AnimatedRecyclerView
import com.ngo.news.R
import com.ngo.news.data.model.CategoryModel
import kotlinx.android.synthetic.main.intrests_fragment.*
import kotlinx.android.synthetic.main.intrests_fragment.swipeLayout
import kotlinx.android.synthetic.main.local_news_fragment.*

class Intrests : Fragment() {

    companion object {
        fun newInstance() = Intrests()
    }

    private lateinit var viewModel: IntrestsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.intrests_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(IntrestsViewModel::class.java)

        val model = ArrayList<CategoryModel>()
        model.apply {
            add(
                CategoryModel(
                    0,
                    "Medical"

                )
            )

            add(
                CategoryModel(
                    1,
                    "Sport"
                )
            )

            add(
                CategoryModel(
                    2,
                    "Polices"
                )
            )

            add(
                CategoryModel(
                    3,
                    "Entertainment"
                )
            )


            add(
                CategoryModel(
                    4,
                    "Community"
                )
            )

            add(
                CategoryModel(
                    5,
                    "Fun"
                )
            )


        }

        val adapter = CategoryAdapter(model)
        categoryRv.layoutManager =
            GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false)
        categoryRv.adapter = adapter

        val recyclerView = AnimatedRecyclerView.Builder(requireContext())
            .orientation(GridLayoutManager.VERTICAL)
            .layoutManagerType(AnimatedRecyclerView.LayoutManagerType.GRID)
            .animation(R.anim.layout_animation_from_bottom)
            .animationDuration(600)
            .reverse(false)
            .build()

        adapter.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()

        swipeLayout.setOnRefreshListener {
            Handler().postDelayed({
                // Stop animation (This will be after 3 seconds)
                swipeLayout.isRefreshing = false
            }, 1500)

        }
    }

}