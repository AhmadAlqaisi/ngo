package com.ngo.news.ui.notifications

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ngo.news.R
import com.ngo.news.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_notifications.*

class NotificationsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        imageView6.setOnClickListener {
            MainActivity.start(this)
            finish()
        }
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, NotificationsActivity::class.java)
            context.startActivity(intent)
        }
    }
}