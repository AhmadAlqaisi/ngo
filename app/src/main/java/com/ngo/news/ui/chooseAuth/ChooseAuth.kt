package com.ngo.news.ui.chooseAuth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ngo.news.R
import com.ngo.news.databinding.ChooseAuthFragmentBinding

class ChooseAuth : Fragment() {

    private lateinit var binding: ChooseAuthFragmentBinding
    private lateinit var viewModel: ChooseAuthViewModel
    private lateinit var nabController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= ChooseAuthFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nabController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this).get(ChooseAuthViewModel::class.java)
        binding.view= this
    }

    fun onClick() = nabController.navigate(R.id.action_chooseAuth_to_signUp)

    fun onClick2() = nabController.navigate(R.id.action_chooseAuth_to_loginFragment)
}