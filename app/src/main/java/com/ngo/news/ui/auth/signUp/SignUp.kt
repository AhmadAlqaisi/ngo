package com.ngo.news.ui.auth.signUp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ngo.news.data.model.SignUpModel
import com.ngo.news.databinding.SignUpFragmentBinding
import kotlinx.android.synthetic.main.sign_up_fragment.*

class SignUp : Fragment() {

    private lateinit var viewModel: SignUpViewModel
    private lateinit var binding: SignUpFragmentBinding
    private lateinit var navController: NavController
    private val model= SignUpModel("", "", "", "", "", "", "")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SignUpFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        spinnerPart.apply {
            lifecycleOwner = requireActivity()
            setOnSpinnerItemSelectedListener<String> { index, text ->
                model.setCountry(text)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)

        binding.let { it.model= model;it.viewModel= viewModel;it.navController= navController }
    }
}