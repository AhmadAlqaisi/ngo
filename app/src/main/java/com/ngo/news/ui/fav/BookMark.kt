package com.ngo.news.ui.fav

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.mlsdev.animatedrv.AnimatedRecyclerView
import com.ngo.news.R
import com.ngo.news.data.manager.CacheManager
import com.ngo.news.data.model.BookMarkModel
import com.ngo.news.data.model.NewsModel
import com.ngo.news.ui.lanPage.LandPageActivity
import kotlinx.android.synthetic.main.book_mark_fragment.*

class BookMark : Fragment() {

    private lateinit var viewModel: BookMarkViewModel
    private lateinit var nabController: NavController

    override fun onAttach(context: Context) {
        if (CacheManager.getUserInfo().id== 0) {
            LandPageActivity.start(requireActivity())

        }

        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.book_mark_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BookMarkViewModel::class.java)
        refresh()
    }

    private fun refresh() {
        viewModel.apply {
            getNewsByType(CacheManager.getUserInfo().id.toString())

            companiesModel.observe(requireActivity(), Observer { it?.let {
                loadNews(model = it)

            } })
        }
    }

    private fun loadNews(model: ArrayList<BookMarkModel>) {
        val adapter = FavAdapter(model, this)
        rvLocalNews1.adapter = adapter
        val recyclerView = AnimatedRecyclerView.Builder(requireContext())
            .orientation(LinearLayoutManager.VERTICAL)
            .layoutManagerType(AnimatedRecyclerView.LayoutManagerType.LINEAR)
            .animation(R.anim.layout_animation_from_bottom)
            .animationDuration(600)
            .reverse(false)
            .build()

        adapter.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nabController = Navigation.findNavController(view)
    }

    fun onClick(modelLocal: BookMarkModel) {
        val bundle = bundleOf("news" to NewsModel(modelLocal.getId(), modelLocal.getTitle(), modelLocal.getDescription(), modelLocal.getImg(), modelLocal.getCategory(), "", modelLocal.getDate()))

        nabController.navigate(R.id.action_fav_to_newsBody, bundle)
    }
}
