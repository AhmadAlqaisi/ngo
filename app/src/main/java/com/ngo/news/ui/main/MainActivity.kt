package com.ngo.news.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.ngo.news.R
import com.ngo.news.data.manager.CacheManager
import com.ngo.news.ui.base.BaseActivity
import com.ngo.news.ui.notifications.NotificationsActivity
import kotlinx.android.synthetic.main.home_page.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_page)

        instance= this

        val navController= Navigation.findNavController(this, R.id.content_frame)
        NavigationUI.setupWithNavController(bottomNavigationView, navController)

        v3.setOnClickListener { NotificationsActivity.start(this) }
        imageView6.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            } else {
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }

        val navController2= Navigation.findNavController(this, R.id.content_frame)
        NavigationUI.setupWithNavController(nav_view, navController2)

    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()

        val header: View = nav_view.getHeaderView(0)
        header.findViewById<TextView>(R.id.userName).text= "${CacheManager.getUserInfo().firstname} ${CacheManager.getUserInfo().lastname}"
        header.findViewById<TextView>(R.id.userEmail).text= CacheManager.getUserInfo().email

    }

    companion object {
        lateinit var instance: MainActivity

        fun start(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }
}