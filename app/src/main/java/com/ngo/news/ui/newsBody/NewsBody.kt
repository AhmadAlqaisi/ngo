package com.ngo.news.ui.newsBody

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.ngo.news.R
import com.ngo.news.data.manager.CacheManager
import com.ngo.news.data.model.NewsModel
import com.ngo.news.databinding.NewsBodyFragmentBinding
import com.ngo.news.ui.lanPage.LandPageActivity

class NewsBody : Fragment() {

    private lateinit var binding: NewsBodyFragmentBinding
    private lateinit var viewModel: NewsBodyViewModel

    lateinit var model: NewsModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= NewsBodyFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NewsBodyViewModel::class.java)

        model = requireArguments().getParcelable("news")!!

        if (model.getImg().isNotEmpty()) {
            Glide.with(this).load("http://globalhealth-news.com/img/${model.getImg()}".replace(" ", "%20")).into(binding.image)
            Glide.with(this).load("http://globalhealth-news.com/img/${model.getImg()}".replace(" ", "%20")).into(binding.smallImage)
        } else { Glide.with(this).load(R.drawable.ic_internet2).into(binding.smallImage) }

        binding.body.findViewById<TextView>(R.id.textView7).text= model.getCategory()
        binding.body.findViewById<TextView>(R.id.textView112).text= model.getTitle()
        binding.toolbar.title= model.getTitle()
        binding.body.findViewById<TextView>(R.id.testsdgdf).text= model.getDescription()
        binding.body.findViewById<TextView>(R.id.textView11).text= model.getDate()
        binding.body.findViewById<ImageView>(R.id.bookMark).setOnClickListener {
            if (CacheManager.getUserInfo().id== 0) {
                LandPageActivity.start(requireActivity())

            } else {
                viewModel.addToFavorite(newsId = model.getId(), context = requireContext())

            }
        }

        binding.body.findViewById<ImageView>(R.id.share).setOnClickListener {
            val myIntent = Intent(Intent.ACTION_SEND)
            myIntent.type = "text/plain"
            val shareSub = model.getTitle()
            val shareBody = "From NGOApp\n${model.getDescription()}"
            myIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
            myIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
            requireActivity().startActivity(Intent.createChooser(myIntent, "ths news shared from NGOApp"))
        }
    }
}
