package com.ngo.news.ui.auth.signUp

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController
import com.nafith.testtask.utils.toast
import com.ngo.news.R
import com.ngo.news.data.model.SignUpModel
import com.ngo.news.data.repository.SignUpRepository
import com.ngo.news.utils.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SignUpViewModel (application: Application) : AndroidViewModel(application)  {

    private lateinit var repository: SignUpRepository

    val isLoading = ObservableField(false)
    var isEmail= ObservableField(false)
    var isFirstName= ObservableField(false)
    var isLastName= ObservableField(false)
    var isEmployer= ObservableField(false)
    var isAffiliation= ObservableField(false)
    var isPassword= ObservableField(false)

    private val compositeDisposable = CompositeDisposable()

    fun back(navController: NavController) {
        navController.navigate(R.id.action_signUp_to_chooseAuth)
    }

    fun signIn(navController: NavController) {
        navController.navigate(R.id.action_signUp_to_loginFragment)
    }

    fun signUp(model: SignUpModel, context: Context, navController: NavController) {
        repository= SignUpRepository()

        isLoading.set(true)

        when {
            model.getFirstName().isEmpty() -> { isLoading.set(false); isFirstName.set(true) }
            model.getLastName().isEmpty() -> { isLoading.set(false); isLastName.set(true) }
            model.getEmail().isEmpty() -> { isLoading.set(false); isEmail.set(true) }
            model.getCountry().isEmpty() -> { isLoading.set(false); context.getString(R.string.country_empty_error).toast(context) }
            model.getEmployer().isEmpty() -> { isLoading.set(false); isEmployer.set(true) }
            model.getAffiliation().isEmpty() -> { isLoading.set(false); isAffiliation.set(true) }
            model.getPassword().isEmpty() -> { isLoading.set(false); isPassword.set(true) }

            else -> {

                compositeDisposable += repository
                    .signUp(model = model)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        isLoading.set(false)
                        Log.d("dafsfsdfs", it.toString())
                        if (it== 1) {
                            navController.navigate(R.id.action_signUp_to_loginFragment)

                        } else {
                            context.getString(R.string.email_exist).toast(context)
                        }

                    }, {
                        isLoading.set(false)
                        Log.d("error", it.message.toString())

                    })
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}