package com.ngo.news.ui.fav

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import androidx.recyclerview.widget.RecyclerView
import com.ngo.news.data.model.BookMarkModel
import com.ngo.news.databinding.FavItemBinding
import java.util.*

class FavAdapter(private var modelListLocal: ArrayList<BookMarkModel>, private var view: BookMark) :
    RecyclerView.Adapter<FavAdapter.ViewHolder>() {

    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FavItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = modelListLocal.get(position)
        holder.mainItemBinding.model = model
        holder.mainItemBinding.view = view
        holder.mainItemBinding.executePendingBindings()
        setAnimation(holder.itemView, position)

    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val anim = ScaleAnimation(
                0.0f,
                1.0f,
                0.0f,
                1.0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            anim.duration = Random().nextInt(501).toLong()//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim)
            lastPosition = position
        }
    }

    override fun getItemCount() = modelListLocal.size

    inner class ViewHolder(val mainItemBinding: FavItemBinding) :
        RecyclerView.ViewHolder(mainItemBinding.root)
}
