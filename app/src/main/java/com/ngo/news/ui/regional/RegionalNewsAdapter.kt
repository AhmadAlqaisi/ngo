package com.ngo.news.ui.regional

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import androidx.recyclerview.widget.RecyclerView
import com.ngo.news.data.model.RegModel
import com.ngo.news.databinding.NwesItemBinding
import com.ngo.news.databinding.RegionalItemBinding
import com.ngo.news.ui.home.LocalNews
import com.ngo.news.ui.model.wrapper.Blog
import java.util.*

class RegionalNewsAdapter(
    private var modelList: ArrayList<RegModel>?,
    private var view: RegionalFragment
) :
    RecyclerView.Adapter<RegionalNewsAdapter.ViewHolder>() {
    private var lastPosition = -1

    var context: Context? = null
    var list: ArrayList<Blog>? = null

    fun RecyclerAnimationAdapter(
        context: Context?,
        personList: ArrayList<Blog>?
    ) {
        this.context = context
        list = personList
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RegionalItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = modelList?.get(position)
        holder.mainItemBinding.model = model
        holder.mainItemBinding.view = view
        holder.mainItemBinding.executePendingBindings()
        setAnimation(holder.itemView, position)


    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val anim = ScaleAnimation(
                0.0f,
                1.0f,
                0.0f,
                1.0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            anim.setDuration(
                Random().nextInt(501).toLong()
            )//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim)
            lastPosition = position
        }
    }

    override fun getItemCount() = modelList?.size!!

    inner class ViewHolder(val mainItemBinding: RegionalItemBinding) :
        RecyclerView.ViewHolder(mainItemBinding.root)


}
