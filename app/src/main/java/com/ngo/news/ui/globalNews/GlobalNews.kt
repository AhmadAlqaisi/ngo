package com.ngo.news.ui.globalNews

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.mlsdev.animatedrv.AnimatedRecyclerView
import com.ngo.news.R
import com.ngo.news.data.model.Category
import com.ngo.news.data.model.GlobalNewsModel
import com.ngo.news.data.model.NewsModel
import com.ngo.news.databinding.GlobalNewsFragmentBinding
import com.ngo.news.ui.home.LocalNewsViewModel
import java.util.*
import kotlin.collections.ArrayList

class GlobalNews : Fragment() {

    private lateinit var binding: GlobalNewsFragmentBinding
    private lateinit var nabController: NavController
    private lateinit var viewModel: LocalNewsViewModel

    private var check= 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= GlobalNewsFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LocalNewsViewModel::class.java)

        getCategory()
        searchTextWatcher()
        refresh(0)
        binding.swipeLayout.setOnRefreshListener {
            Handler().postDelayed({
                // Stop animation (This will be after 3 seconds)
                binding.swipeLayout.isRefreshing = false
            }, 1500)
        }
    }

    fun category(category: ArrayList<Category>) {
        val array= ArrayList<String>()

        if (Locale.getDefault().language == "ar") array.add("كل الأخبار")
        else array.add("all news")

        for (i in 0 until category.size) {
            array.add(category[i].getTitle())
        }

        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, array)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinner1.adapter = adapter

        binding.spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (check!=0) {
                    if (position== 0) {refresh(0)}
                    else refresh(category[position- 1].getId())
                }

            }
        }
    }

    private fun refresh(c:Int) {
        viewModel.apply {

            if (c== 0) {
                getNewsByType2(type = "globle", category = "")

            } else {
                getNewsByType2(type = "globle", category = c.toString())

            }

            companiesModel2.observe(requireActivity(), Observer { it?.let {
                loadNews(model = it)

            } })
        }
    }

    private fun searchTextWatcher() {
        binding.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isEmpty()){
                    getCategory()

                }else{
                    if (check!= 0) search(s.toString())

                }
            }
        })
    }

    private fun search(c: String) {
        viewModel.apply {
            Log.d("safdg11111", c)

            searchG(type = "globle", text = c)

        }
    }

    private fun getCategory() {
        viewModel.apply {
            getCategories()
            companiesModelCategory.observe(requireActivity(), Observer { it?.let {
                category(it)

            } })
        }
    }

    private fun loadNews(model: ArrayList<GlobalNewsModel>) {
        Log.d("jyghhfedgf", model.size.toString())
        val adapter = GlobalNewsAdapter(model, this)
        binding.rvLocalNews.adapter = adapter
        val recyclerView = AnimatedRecyclerView.Builder(requireContext())
            .orientation(LinearLayoutManager.VERTICAL)
            .layoutManagerType(AnimatedRecyclerView.LayoutManagerType.LINEAR)
            .animation(R.anim.layout_animation_from_bottom)
            .animationDuration(600)
            .reverse(false)
            .build()

        adapter.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()

        check= 1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nabController = Navigation.findNavController(view)
    }

    fun onClick(modelLocal: GlobalNewsModel) {
        val bundle = bundleOf("news" to NewsModel(modelLocal.getId(), modelLocal.getTitle(), modelLocal.getDescription(), modelLocal.getImg(), modelLocal.getCategory(), modelLocal.getCountry(), modelLocal.getDate()))

        nabController.navigate(R.id.action_globalNews_to_newsBody, bundle)
    }
}
