package com.ngo.news.ui.model.wrapper

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class Wrapper {
    @SerializedName("blogs")
    @Expose
    var blogs: ArrayList<Blog>? = null
}