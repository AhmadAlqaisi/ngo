package com.ngo.news.ui.model.wrapper

import android.view.View
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import kotlinx.android.synthetic.main.nwes_item.view.*


class Blog {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("image")
    @Expose
    var image: String? = null

    @SerializedName("content")
    @Expose
    var content: String? = null


    companion object {
        @BindingAdapter("loadRegionalImage")
        @JvmStatic
        fun loadImage(view: View, image: String) {
            Glide.with(view.context)
                .load("http://thurayaalnajjar.info/blog/${image}" )
                .fitCenter()
                .into(view.imgs)

        }
    }
}