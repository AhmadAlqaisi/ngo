package com.ngo.news.ui.newsBody

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.nafith.testtask.utils.toast
import com.ngo.news.R
import com.ngo.news.data.repository.FavoriteRepository
import com.ngo.news.utils.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewsBodyViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var repository: FavoriteRepository
    private val compositeDisposable = CompositeDisposable()

    val isLoading = ObservableField(false)

    fun addToFavorite(newsId: Int, context: Context) {
        isLoading.set(true)
        repository= FavoriteRepository()
        compositeDisposable += repository
            .addToFavorite(newsId = newsId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                isLoading.set(false)
                if (it== 1) {
                    context.getString(R.string.added_to_favorite).toast(context)

                } else {
                    context.getString(R.string.exist_in_favorite).toast(context)

                }

            }, {
                isLoading.set(false)
                Log.d("DSbfsdbfdbf", it.message.toString())

            })
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}