package com.ngo.news.ui.language

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.akexorcist.localizationactivity.core.LanguageSetting
import com.ngo.news.SplashActivity
import com.ngo.news.databinding.ActivityLanguageBinding
import com.ngo.news.ui.main.MainActivity
import java.util.*

class LanguageActivity : Fragment() {

    private lateinit var binding: ActivityLanguageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ActivityLanguageBinding.inflate(inflater, container, false)
        binding.let { it.view= this;  }//it.back.setOnClickListener { onBackPressed() }

        return binding.root
    }


    fun switchLanguage(context: Context) {
        if (Locale.getDefault().language == "en") LanguageSetting.setLanguage(context, Locale("ar"))
        else LanguageSetting.setLanguage(context, Locale("en"))

        SplashActivity.start(MainActivity.instance)
        MainActivity.instance.finishAffinity()
    }
}
