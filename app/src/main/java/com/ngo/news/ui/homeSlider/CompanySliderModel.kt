package com.ngo.news.ui.homeSlider

import com.google.gson.annotations.SerializedName

data class CompanySliderModel(
    @SerializedName("id")
    var id: Int,
    @SerializedName("market_id")
    var market_id: Int,
    @SerializedName("image")
    var image: String
)
