package com.ngo.news

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.ngo.news.ui.base.BaseActivity
import com.ngo.news.ui.main.MainActivity

class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        CacheManager.clearAll()
        Handler().postDelayed({
            MainActivity.start(this)
            finishAffinity()
//            if (CacheManager.getUserInfo().id== 0) {
//                LandPageActivity.start(this)
//                finishAffinity()
//
//            } else {
//                MainActivity.start(this)
//                finishAffinity()
//
//            }
        }, 2000)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, SplashActivity::class.java)
            context.startActivity(intent)
        }
    }
}
