package com.ngo.news.data.network

import com.google.gson.GsonBuilder
import com.ngo.news.utils.App
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

object Api {
    private var httpCacheDirectory: File = File(App.instance.cacheDir, "offlineCache")

    //10 MB
    private var cache = Cache(httpCacheDirectory, 10 * 1024 * 1024)
    var BASE_URL: String = "http://globalhealth-news.com/apis/"
//    var BASE_URL: String = "http://thurayaalnajjar.info/blog/"

    val getData: ApiInterface
        get() {

            val json = GsonBuilder()
                .setLenient()
                .create()
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor)
                .cache(cache)

                .addInterceptor { chain ->
                    var request = chain.request()
                    request = if (App.instance.isNetworkAvailable())
                        request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                    else
                        request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                    chain.proceed(request)
                }

                .build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(json))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
}

