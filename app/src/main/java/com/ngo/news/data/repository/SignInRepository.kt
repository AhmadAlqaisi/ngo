package com.ngo.news.data.repository

import com.ngo.news.data.model.SignInModel
import com.ngo.news.data.model.UserInfoModel
import com.ngo.news.data.network.Api
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SignInRepository {

    fun signIn(model: SignInModel) : Observable<UserInfoModel> {
        return Api.getData.signIn(email = model.getEmail(), password = model.getPassword())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
