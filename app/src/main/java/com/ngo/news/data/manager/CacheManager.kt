package com.ngo.news.data.manager

import com.ngo.news.data.model.UserInfoModel
import io.paperdb.Paper

object CacheManager {

    //keys
    private var keyFcmToken= "fcmToken"
    private var keyUserInfo= "userInfo"

    private var fcmToken: String= ""
    private var userInfo: UserInfoModel = UserInfoModel(id = 0, email = "", firstname = "", lastname = "", country = "", employer = "", affiliation = "", image = "", password = "")

    init {
//        fcmToken= Paper.book().read(keyFcmToken, "")
        userInfo = Paper.book().read(keyUserInfo, UserInfoModel(id = 0, email = "", firstname = "", lastname = "", country = "", employer = "", affiliation = "", image = "", password = ""))

    }

    fun clearUserInfo() { Paper.book().delete(keyUserInfo) }
    fun clearFcmToken() { Paper.book().delete(keyFcmToken) }
    fun clearAll() { Paper.book().destroy() }

    //------------ Get Cache Value ------------//

    fun getUserInfo(): UserInfoModel = userInfo
    fun getFcmToken(): String = fcmToken


    //------------ Set Cache Value ------------//

    fun setUserInfo(userInfo: UserInfoModel) {
        this.userInfo = userInfo
        Paper.book().write(keyUserInfo, userInfo)
    }

    fun setFcmToken(fcmToken: String) {
        this.fcmToken = fcmToken
        Paper.book().write(keyFcmToken, fcmToken)
    }
}