package com.ngo.news.data.model

import android.os.Parcelable
import android.util.Log
import android.view.View
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import com.ngo.news.R
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.nwes_item.view.*

@Parcelize
data class NewsModel(
    @SerializedName("id")
    private var id: Int,
    @SerializedName("title")
    private var title: String,
    @SerializedName("body")
    private var description: String,
    @SerializedName("image")
    private var img: String,
    @SerializedName("categoryId")
    private var category: String,
    @SerializedName("country")
    private var country: String,
    @SerializedName("date")
    private var date: String
): Parcelable {

    fun getId(): Int {
        return id
    }

    fun getDescription(): String {
        return description
    }

    fun getImg(): String {
        return img
    }

    fun getTitle(): String {
        return title
    }

    fun getCategory(): String {
        return category
    }

    fun getCountry(): String {
        return country
    }

    fun getDate(): String {
        return date
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun setImg(img: String) {
        this.img = img
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun setCategory(category: String) {
        this.category = category
    }

    fun setCountry(country: String) {
        this.country = country
    }

    fun setDate(date: String) {
        this.date = date
    }


    companion object {
        @BindingAdapter("loadNewsImage2")
        @JvmStatic
        fun loadImage(view: View, image: String) {
            Log.d("azsdftyujhgvb", image)
            Log.d("azsdftyujhgvb", "http://globalhealth-news.com/img/$image".replace(" ", "%20"))
            if (image == "") {
                Glide.with(view.context)
                    .load(R.drawable.defult)
                    .fitCenter()
                    .into(view.imgs)
            }else{
                Glide.with(view.context)
                    .load("http://globalhealth-news.com/img/$image".replace(" ", "%20"))
                    .fitCenter()
                    .into(view.imgs)
            }
        }
    }
}