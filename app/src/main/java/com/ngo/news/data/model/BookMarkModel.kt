package com.ngo.news.data.model

import android.os.Parcelable
import android.view.View
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.fav_item.view.*

@Parcelize
data class BookMarkModel(
    @SerializedName("id")
    private var id: Int,
    @SerializedName("title")
    private var title: String,
    @SerializedName("body")
    private var description: String,
    @SerializedName("type")
    private var type: String,
    @SerializedName("categoryId")
    private var categoryId: String,
    @SerializedName("image")
    private var img: String,
    @SerializedName("date")
    private var date: String
) : Parcelable {

    fun getId(): Int {
        return id
    }

    fun getDescription(): String {
        return description
    }

    fun getImg(): String {
        return img
    }

    fun getTitle(): String {
        return title
    }

    fun getCategory(): String {
        return categoryId
    }

    fun getDate(): String {
        return date
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun setImg(img: String) {
        this.img = img
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun setCategory(category: String) {
        this.categoryId = category
    }

    fun setDate(date: String) {
        this.date = date
    }


    companion object {
        @BindingAdapter("favImage")
        @JvmStatic
        fun loadImage(view: View, image: String) {
            Glide.with(view.context)
                .load("http://globalhealth-news.com/img/$image".replace(" ", "%20"))
                .fitCenter()
                .into(view.imgs2)

        }
    }
}