package com.ngo.news.data.repository

import com.ngo.news.data.manager.CacheManager
import com.ngo.news.data.network.Api
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FavoriteRepository {

    fun addToFavorite(newsId: Int) : Observable<Int> {
        return Api.getData.addToFavorite(newsId = newsId, userId = CacheManager.getUserInfo().id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
