package com.ngo.news.data.repository

import com.ngo.news.data.model.SignUpModel
import com.ngo.news.data.network.Api
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SignUpRepository {

    fun signUp(model: SignUpModel) : Observable<Int> {
        return Api.getData.signUp(email = model.getEmail(), firstname = model.getFirstName(), lastname = model.getLastName(), country = model.getCountry(), employer = model.getEmployer(), affiliation = model.getAffiliation(), password = model.getPassword())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
