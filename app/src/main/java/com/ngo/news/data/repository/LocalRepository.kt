package com.ngo.news.data.repository

import com.ngo.news.data.model.Category
import com.ngo.news.data.model.GlobalNewsModel
import com.ngo.news.data.model.NewsModel
import com.ngo.news.data.model.RegModel
import com.ngo.news.data.network.Api
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class LocalRepository {

    fun getNewsByType(type: String,category:String,country:String) : Observable<ArrayList<NewsModel>> {
        return Api.getData.getFilter(lang = Locale.getDefault().language, type = type,category = category,country = country)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun search(type: String,text:String) : Observable<ArrayList<NewsModel>> {
        return Api.getData.search(lang = Locale.getDefault().language, type = type,title = text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchG(type: String,text:String) : Observable<ArrayList<GlobalNewsModel>> {
        return Api.getData.searchG(lang = Locale.getDefault().language, type = type,title = text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchR(type: String,text:String) : Observable<ArrayList<RegModel>> {
        return Api.getData.searchR(lang = Locale.getDefault().language, type = type,title = text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getNewsByType2(type: String,category:String) : Observable<ArrayList<GlobalNewsModel>> {
        return Api.getData.getFilter2(lang = Locale.getDefault().language, type = type,category = category)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getNewsByType3(type: String,category:String,country:String) : Observable<ArrayList<RegModel>> {
        return Api.getData.getFilter3(lang = Locale.getDefault().language, type = type,category = category,country = country)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getCategories() : Observable<ArrayList<Category>> {
        return Api.getData.getCategories(lang = Locale.getDefault().language)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}
