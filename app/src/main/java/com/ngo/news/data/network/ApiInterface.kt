package com.ngo.news.data.network

import com.ngo.news.data.model.*
import io.reactivex.Observable
import retrofit2.http.*

interface ApiInterface {

    @GET("getNewsByType.php")
    fun getNewsByType(
        @Query("lang") lang: String,
        @Query("type") type: String
    ): Observable<ArrayList<NewsModel>>



    @GET("getNewsByType.php")
    fun getNewsByType2(
        @Query("lang") lang: String,
        @Query("type") type: String
    ): Observable<ArrayList<NewsModel>>


    @GET("getCategories.php")
    fun getCategories(
        @Query("lang") lang: String
    ): Observable<ArrayList<Category>>


    @GET("getRegCountries.php")
    fun getRegCountries(
        @Query("lang") lang: String
    ): Observable<ArrayList<CountryReg>>

    @GET("search.php")
    fun search(
        @Query("lang") lang: String,
        @Query("type") type: String,
        @Query("title") title: String
    ): Observable<ArrayList<NewsModel>>

    @GET("searchGlobal.php")
    fun searchG(
        @Query("lang") lang: String,
        @Query("type") type: String,
        @Query("title") title: String
    ): Observable<ArrayList<GlobalNewsModel>>

    @GET("searchRegional.php")
    fun searchR(
        @Query("lang") lang: String,
        @Query("type") type: String,
        @Query("title") title: String
    ): Observable<ArrayList<RegModel>>

    @GET("getFavorite.php")
    fun getFavorite(
        @Query("lang") lang: String,
        @Query("id") id: String
    ): Observable<ArrayList<BookMarkModel>>

    @GET("getFilter.php")
    fun getFilter(
        @Query("lang") lang: String,
        @Query("type") type: String,
        @Query("category") category: String,
        @Query("country") country: String
    ): Observable<ArrayList<NewsModel>>

    @GET("getFilterGlobal.php")
    fun getFilter2(
        @Query("lang") lang: String,
        @Query("type") type: String,
        @Query("category") category: String
    ): Observable<ArrayList<GlobalNewsModel>>

    @GET("getFilterRegnal.php")
    fun getFilter3(
        @Query("lang") lang: String,
        @Query("type") type: String,
        @Query("category") category: String,
        @Query("country") country: String
    ): Observable<ArrayList<RegModel>>

    @FormUrlEncoded
    @POST("signUp.php")
    fun signUp(
        @Field("email") email: String,
        @Field("firstname") firstname: String,
        @Field("lastname") lastname: String,
        @Field("country") country: String,
        @Field("employer") employer: String,
        @Field("affiliation") affiliation: String,
        @Field("password") password: String
    ): Observable<Int>

    @FormUrlEncoded
    @POST("signIn.php")
    fun signIn(
        @Field("email") email: String,
        @Field("password") password: String
    ): Observable<UserInfoModel>

    @FormUrlEncoded
    @POST("addToFavorite.php")
    fun addToFavorite(
        @Field("newsId") newsId: Int,
        @Field("userId") userId: Int
    ): Observable<Int>

}
