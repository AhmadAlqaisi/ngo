package com.ngo.news.data.fcm

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.ngo.news.R
import com.ngo.news.SplashActivity
import com.ngo.news.data.manager.CacheManager
import org.json.JSONObject
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val params = remoteMessage.data
        val `object` = JSONObject(params as Map<*, *>)
        Log.d("JSON OBJECT", `object`.toString())

        val title: String
        val body: String

        if (`object`.toString().isEmpty() || `object`.toString()== "{}") {
            Log.d("remoteMessage", remoteMessage.notification!!.title!! + ", "+ remoteMessage.notification!!.body!!)
            title= remoteMessage.notification!!.title!!
            body= remoteMessage.notification!!.body!!

        } else {
            title= `object`.getString("title")
            body= `object`.getString("message")

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            sendSimpleNotificationOreoPush(title, body)

        } else {
            sendNotification(title, body)

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun sendSimpleNotificationOreoPush(title: String, desc: String) {

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val id = "id_product"
        val name = "Badger"
        @SuppressLint("WrongConstant") val mChannel = NotificationChannel(id, name, NotificationManager.IMPORTANCE_MAX)
        mChannel.description = desc
        mChannel.enableLights(true)
        notificationManager.createNotificationChannel(mChannel)

        val intent = if (desc.contains("You have an order")) {
            Log.d("asdfgbv", desc)

            Intent(this, SplashActivity::class.java)

        } else {
            Log.d("asdfgbv", desc)

            Intent(this, SplashActivity::class.java)

        }

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra("isFromFcm", true)

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val random = Random()
        val notifyID = random.nextInt(9999 - 1000) + 1000
        val notificationBuilder = NotificationCompat.Builder(applicationContext, "id_product")

            .setSmallIcon(notificationIcon) //your app icon
            .setBadgeIconType(notificationIcon) //your app icon
            .setChannelId(id)
            .setContentTitle(title)
            .setAutoCancel(true).setContentIntent(pendingIntent)
            .setNumber(1)
            .setContentText(desc)
            .setSound(alarmSound)
            .setWhen(System.currentTimeMillis())
        notificationManager.notify(notifyID, notificationBuilder.build())


    }

    private fun sendNotification(title: String, desc: String) {

        val intent = if (desc.contains("You have an order")) {
            Log.d("asdfgbv", desc)

            Intent(this, SplashActivity::class.java)

        } else {
            Log.d("asdfgbv", desc)

            Intent(this, SplashActivity::class.java)

        }

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra("isFromFcm", true)

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val mBuilder = NotificationCompat.Builder(this)

        val s = NotificationCompat.BigTextStyle()
        s.setBigContentTitle(desc)
        s.setSummaryText(title)

        val notification: Notification
        notification = mBuilder.setSmallIcon(R.drawable.ic_ubuntu)
            .setTicker(title)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setContentTitle(title)
            .setSmallIcon(R.drawable.ic_ubuntu)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_ubuntu))
            .setContentText(desc)
            .setStyle(NotificationCompat.BigTextStyle().bigText(desc))
            .build()

        val random = Random()
        val m = random.nextInt(9999 - 1000) + 1000

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(m, notification)


    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.d("fcmToken", p0)

        CacheManager.setFcmToken(p0)
    }
    companion object {
        private val notificationIcon: Int
            get() {
//                val useWhiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
//                return if (useWhiteIcon) R.drawable.logo_points else R.drawable.logo_points

                return R.drawable.ic_ubuntu
            }
    }
}
