package com.ngo.news.data.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.ngo.news.BR
import com.ngo.news.R
import com.ngo.news.utils.App

data class SignInModel(private var email: String, private var password: String) : BaseObservable() {

    @Bindable
    fun getEmail(): String {
        return email
    }

    @Bindable
    fun getPassword(): String {
        return password
    }

    @Bindable
    fun setEmail(email: String) {
        this.email = email
        notifyPropertyChanged(BR.email)
        notifyPropertyChanged(BR.emailError)
    }

    @Bindable
    fun setPassword(password: String) {
        this.password = password
        notifyPropertyChanged(BR.password)
        notifyPropertyChanged(BR.passwordError)
    }

    @Bindable
    fun getEmailError(): String? {
        return if (password.isEmpty()) {
            App.instance.getString(R.string.email_empty_error)
        } else {
            null
        }
    }

    @Bindable
    fun getPasswordError(): String? {
        return if (password.isEmpty()) {
            App.instance.getString(R.string.password_empty_error)
        } else {
            null
        }
    }
}