package com.ngo.news.data.model

import android.os.Parcelable
import android.view.View
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.nwes_item.view.*

@Parcelize
data class CategoryModel(
    private var id: Int?,
    private var description: String?
): Parcelable {

    fun getId(): Int? {
        return id
    }

    fun getDescription(): String? {
        return description
    }


    fun setId(id: Int?) {
        this.id = id
    }

    fun setDescription(description: String?) {
        this.description = description
    }




}