package com.ngo.news.data.repository

import com.ngo.news.data.model.BookMarkModel
import com.ngo.news.data.network.Api
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class FavRepository {

    fun getNewsByType(id: String) : Observable<ArrayList<BookMarkModel>> {
        return Api.getData.getFavorite(lang = Locale.getDefault().language, id = id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}
