package com.ngo.news.data.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.ngo.news.BR
import com.ngo.news.R
import com.ngo.news.utils.App

data class SignUpModel(private var email: String, private var firstName: String, private var lastName: String, private var country: String, private var employer: String, private var affiliation: String, private var password: String) : BaseObservable() {

    @Bindable
    fun getEmail(): String {
        return email
    }

    @Bindable
    fun getFirstName(): String {
        return firstName
    }

    @Bindable
    fun getLastName(): String {
        return lastName
    }

    @Bindable
    fun getCountry(): String {
        return country
    }

    @Bindable
    fun getEmployer(): String {
        return employer
    }

    @Bindable
    fun getAffiliation(): String {
        return affiliation
    }

    @Bindable
    fun getPassword(): String {
        return password
    }

    @Bindable
    fun setEmail(email: String) {
        this.email = email
        notifyPropertyChanged(BR.email)
        notifyPropertyChanged(BR.emailError)
    }

    @Bindable
    fun setFirstName(firstName: String) {
        this.firstName = firstName
        notifyPropertyChanged(BR.firstName)
        notifyPropertyChanged(BR.firstNameError)
    }

    @Bindable
    fun setLastName(lastName: String) {
        this.lastName = lastName
        notifyPropertyChanged(BR.lastName)
        notifyPropertyChanged(BR.lastNameError)
    }

    @Bindable
    fun setCountry(country: String) {
        this.country = country
        notifyPropertyChanged(BR.country)
        notifyPropertyChanged(BR.countryError)
    }

    @Bindable
    fun setEmployer(employer: String) {
        this.employer = employer
        notifyPropertyChanged(BR.employer)
        notifyPropertyChanged(BR.employerError)
    }

    @Bindable
    fun setAffiliation(affiliation: String) {
        this.affiliation = affiliation
        notifyPropertyChanged(BR.affiliation)
        notifyPropertyChanged(BR.affiliationError)
    }

    @Bindable
    fun setPassword(password: String) {
        this.password = password
        notifyPropertyChanged(BR.password)
        notifyPropertyChanged(BR.passwordError)
    }

    @Bindable
    fun getEmailError(): String? {
        return if (password.isEmpty()) {
            App.instance.getString(R.string.email_empty_error)
        } else {
            null
        }
    }

    @Bindable
    fun getFirstNameError(): String? {
        return if (firstName.isEmpty()) {
            App.instance.getString(R.string.first_name_empty_error)
        } else {
            null
        }
    }

    @Bindable
    fun getLastNameError(): String? {
        return if (lastName.isEmpty()) {
            App.instance.getString(R.string.last_name_empty_error)
        } else {
            null
        }
    }

    @Bindable
    fun getCountryError(): String? {
        return if (country.isEmpty()) {
            App.instance.getString(R.string.country_empty_error)
        } else {
            null
        }
    }

    @Bindable
    fun getEmployerError(): String? {
        return if (employer.isEmpty()) {
            App.instance.getString(R.string.employer_empty_error)
        } else {
            null
        }
    }

    @Bindable
    fun getAffiliationError(): String? {
        return if (affiliation.isEmpty()) {
            App.instance.getString(R.string.affiliation_empty_error)
        } else {
            null
        }
    }

    @Bindable
    fun getPasswordError(): String? {
        return if (password.isEmpty()) {
            App.instance.getString(R.string.password_empty_error)
        } else {
            null
        }
    }
}