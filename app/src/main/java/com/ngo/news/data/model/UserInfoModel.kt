package com.ngo.news.data.model

import com.google.gson.annotations.SerializedName

data class UserInfoModel(
    @SerializedName("id")
    var id: Int,
    @SerializedName("email")
    var email: String,
    @SerializedName("firstname")
    var firstname: String,
    @SerializedName("lastname")
    var lastname: String,
    @SerializedName("country")
    var country: String,
    @SerializedName("employer")
    var employer: String,
    @SerializedName("affiliation")
    var affiliation: String,
    @SerializedName("image")
    var image: String,
    @SerializedName("password")
    var password: String
)
