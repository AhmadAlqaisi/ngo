package com.ngo.news.data.model

import android.os.Parcelable
import android.util.Log
import android.view.View
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import com.ngo.news.data.network.Api
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.nwes_item.view.*

@Parcelize
data class CountryReg(
    @SerializedName("id")
    private var id: Int,
    @SerializedName("name")
    private var name: String

): Parcelable {

    fun getId(): Int {
        return id
    }


    fun getTitle(): String {
        return name
    }



    fun setId(id: Int) {
        this.id = id
    }

    fun setTitle(title: String) {
        this.name = title
    }

}