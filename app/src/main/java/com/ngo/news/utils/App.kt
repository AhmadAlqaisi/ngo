package com.ngo.news.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.multidex.MultiDex
import io.paperdb.Paper
import java.util.*

class App:Application() {

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    override fun onCreate() {
        super.onCreate()
        instance=this
        Paper.init(this)

        settingMultipleLanguages()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"))
        MultiDex.install(this)
    }

    private fun settingMultipleLanguages() {
        val firstLaunchLocale = Locale("en")
        val arabic = Locale("ar")
        val supportedLocales = HashSet<Locale>()
        supportedLocales.add(firstLaunchLocale)
        supportedLocales.add(arabic)
    }

    companion object {
        lateinit var instance: App
            private set
    }

}