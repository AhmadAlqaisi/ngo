@file:JvmName("ViewUtils")

package com.ngo.news.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText

inline fun EditText.onTextChanged(crossinline callback: (text: CharSequence?) -> Unit) {
    val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            callback(s)
        }
    }
    addTextChangedListener(textWatcher)
}


fun View.show() {
    post {
        visibility = View.VISIBLE
    }
}

fun View.hide() {
    post {
        visibility = View.GONE
    }
}

fun View.onClick(callback: (View) -> Unit) {
    setOnClickListener(callback)
}
